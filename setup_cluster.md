# install kubernetes

```
curl -sfL https://get.k3s.io | INSTALL_K3S_EXEC="server --no-deploy=traefik" sh -s -
```

# Copy k3s config

```
mkdir $HOME/.kube
sudo cp /etc/rancher/k3s/k3s.yaml $HOME/.kube/config
sudo chmod 644 $HOME/.kube/config
```

# deploy istio

```
curl -L https://istio.io/downloadIstio | ISTIO_VERSION=1.14.3 TARGET_ARCH=x86_64 sh -
cd istio-1.14.3/
chmod +x bin/istioctl
export PATH=$PWD/bin:$PATH
```

> disable sidecar injection in case installation fails

```
kubectl label namespace istio-system istio-injection=disabled --overwrite
```

> install istio with default profile for ingressgateway
```
istioctl install --set profile=default -y
```

# enable sidecar injection

```
kubectl label namespace default istio-injection=enabled --overwrite
```

# deploy apps

```
kubectl apply -f immudb-leader/
kubectl apply -f immudb-reader/
```

# deploy prometheus

```
export PROMETEHUS_VERSION="v0.58.0"
kubectl apply \
-f https://raw.githubusercontent.com/prometheus-operator/prometheus-operator/${PROMETEHUS_VERSION}/example/prometheus-operator-crd/monitoring.coreos.com_prometheuses.yaml --force-conflicts=true --server-side
kubectl apply -f https://raw.githubusercontent.com/prometheus-operator/prometheus-operator/master/bundle.yaml
kubectl apply -f prometheus/
```

# deploy grafana

```
kubectl label namespace monitoring istio-injection=enabled
kubectl create namespace monitoring
kubectl apply -f grafana/
```

# deploy gateways

```
kubectl apply -f gateways/
```

# import dashboard

> post install job for importing dashboard?
> configmap and secrets?

https://github.com/codenotary/immudb/blob/master/tools/monitoring/grafana-dashboard.json